const express = require("express");
const bodyPareser = require("body-parser");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const app = express();


mongoose.connect('mongodb://localhost:27017/twitter');


app.use(bodyPareser.json());
app.use(bodyPareser.urlencoded({extended: true}));

//Users
const userSchema = new Schema({
    name: String,
    age: Number,
});

const User = mongoose.model('users', userSchema);

app.post('/users', (req, res) => {
    const user = new User({
        name: req.body.name,
        age: req.body.age,
    });
    user.save(function (error) {
        if (error) {
            res.send(error)
        }

        res.send("User added successfully")
    });
});


app.get('/users', (req, res) => {
    User.find({}, function (error, docs) {
        if (error) {
            res.send(error)
        }

        res.send(docs)
    })
});

app.get('/users/:id', (req, res) => {
    const id = req.params.id;
    User.findById(id, function (error, doc) {
        if (error) {
            res.send(error)
        }

        res.send(doc)
    })
})

app.put('/users/:id', (req, res) => {
    const userData = {
        name: req.body.name,
        age: req.body.age,

    };
    const id = req.params.id;

    User.findByIdAndUpdate(id, userData, {new: true}, function (err, doc) {
        if (err) {
            res.send(err)
        }

        res.send(doc)
    })
})

app.delete('/users/:id', (req, res) => {
    const id = req.params.id;


    User.findByIdAndRemove(req.params.id, function (err, doc) {
        if (!doc) {
            res.send("User does not exists");

        }
        if (err) {
            res.send(err);

        }
        res.send("deleted")
    });
})


app.get('/users/:id/tweets', (req, res) => {
    const id = req.params.id;

    // check if user exists
    User.findById(id, function (err, user) {


        if (!user) {
            return res.send("User does not exists");

        }
        if (err) {
            return res.send(err);
        }
    });

    // get users tweets
    Tweet.find({user_id: id}, function (err, tweets) {
        if (!tweets) {
            return res.send("User does not have any tweet");
        }

        if (err) {
            return res.send(err);
        }

        res.send(tweets)
    });
})


//Tweets
const tweetSchema = new Schema({
    title: String,
    description: String,
    user_id: String
});

const Tweet = mongoose.model('tweets', tweetSchema);

app.post('/tweets', (req, res) => {
    const tweet = new Tweet({
        title: req.body.title,
        description: req.body.description,
        user_id: req.body.user_id,
    });
    tweet.save(function (error) {
        if (error) {
            res.send(error)
        }

        res.send("Tweet saved successfully")
    });
});


app.get('/tweets', (req, res) => {
    Tweet.find({}, function (error, docs) {
        if (error) {
            res.send(error)
        }

        res.send(docs)
    })
});

app.get('/tweets/:id', (req, res) => {
    const id = req.params.id;
    Tweet.findById(id, function (error, doc) {
        if (error) {
            res.send(error)
        }

        res.send(doc)
    })
})

app.put('/tweets/:id', (req, res) => {
    const tweet = new Tweet({
        title: req.body.title,
        description: req.body.description,
    });

    const id = req.params.id;

    Tweet.findByIdAndUpdate(id, tweet, {new: true}, function (err, doc) {
        if (err) {
            res.send(err)
        }

        res.send(doc)
    })
})

app.delete('/tweets/:id', (req, res) => {
    const id = req.params.id;

    Tweet.findByIdAndRemove(req.params.id, function (err, doc) {
        if (!doc) {
            res.send("Tweet does not exists");
        }

        if (err) {
            res.send(err);
        }
        res.send("Tweet deleted successfully")
    });

})


app.listen(3000)
